import React, { useState, useEffect } from 'react';
import { getHomeData } from '../service/home';
import toyota_header_logo from '../assets/toyota_header_logo.svg';

import './Home.css';
const Home = () => {
	const [homeData, setHomeData] = useState({});

	useEffect(() => {
		const fetchData = async () => {
			const res = await getHomeData();
			setHomeData(res);
			console.log(res.placeholders.jssMain[0].fields.title.value);
		};
		fetchData();
	}, []);
	return (
		<div className="home">
			<img src={toyota_header_logo} alt="/" className="logo" />
			{homeData.placeholders && (
				<div>{homeData.placeholders.jssMain[0].fields.title.value}</div>
			)}
			{homeData.placeholders && (
				<img
					className="image"
					src={homeData.placeholders.jssMain[0].fields.imageLink.value.src}
					alt={homeData.placeholders.jssMain[0].fields.imageLink.value.alt}
				/>
			)}
		</div>
	);
};

export default Home;
