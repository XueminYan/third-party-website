import data from '../mockData/data';

export const getHomeData = () =>
	new Promise((resolve, reject) => {
		return setTimeout(() => resolve(data), 200);
	});
