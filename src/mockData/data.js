const data = {
	name: 'home',
	displayName: 'Home',
	placeholders: {
		jssMain: [
			{
				componentName: 'Home',
				fields: {
					title: {
						value:
							'Sitecore Experience Platform + JSS + React + disconnected!!',
					},
					imageLink: {
						value: {
							src: 'https://partsonline-qa.tmca-digital.com.au/static/media/Hero.8bcf520b.jpg',
							alt: 'Sitecore Logo',
						},
					},
				},
			},
		],
	},
};

export default data;
